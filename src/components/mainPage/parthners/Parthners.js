import React from 'react';
import inRating from './partners/inRating_logo.png';
import LeggioAvi from './partners/LeggioAvi.png';
import WTC from './partners/WTC.png';
import Juv from './partners/logoNew.png';
import TopTalk from './partners/toptalk.png';
import rbk from './partners/rbk.svg';
import './parthners.css';

class Parthners extends React.Component {
	render() {
		return (
				<div className="parthners">
					<h1>{this.props.lang === "Eng" ? "Partners" : "Партнеры"}</h1>
					<div className="parthners-container">
						<div className="parthners-container-top">
							<div className='parthners-item'>
								<a href="https://inrating.top/" target="_blank" rel="noopener noreferrer"><img src={inRating} alt="parthners-inRating" className="inrating-logo"/></a>
							</div>
							<div className='parthners-item'>
								<a href="https://www.rbc.ua/" target="_blank" rel="noopener noreferrer"><img src={rbk} alt="parthners-RBK" className="rbk-logo"/></a>
							</div>
							<div className="parthners-item">
								<a href="https://www.leggioavi.com/" target="_blank" rel="noopener noreferrer"><img src={LeggioAvi} alt="parthners-LeggioAvi" className="legio-logo"/></a>
							</div>
						</div>
						<div className="parthners-container-bottom">
							<div>
								<a href="http://worldtopcontest.com/" target="_blank" rel="noopener noreferrer"><img src={WTC} alt="parthners-WTC" className="wtc-logo"/></a>
							</div>
							<div className="talk-logo-div">
								<a href="/" target="_blank" rel="noopener noreferrer"><img src={TopTalk} alt="parthners-WTC" className="talk-logo"/></a>
							</div>
							<div>
								<a href="http://ddm.kiev.ua" target="_blank" rel="noopener noreferrer"><img src={Juv} alt="parthners-WTC" className="juv-logo"/></a>
							</div>
						</div>
					</div>
				</div>
		);
	}
}

export default Parthners;