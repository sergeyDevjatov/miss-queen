import React from 'react';
import girl from './girl_about_bg.png';
import './about.css';

class About extends React.Component {
	render() {
		return (
			<div className="about">
				<div className="about-content">
					<div>
						<div className="about-img">
							<img src={girl} alt="about-girl"/>
						</div>
						<div className="about-text">
							<h1>{this.props.lang === "Eng" ? "About" : "О нас"}</h1>
							<p>{this.props.lang === "Eng" ? "«Miss Queen Europe» is a European competition aimed at promoting the true beauty, femininity and nobility." : "«Miss Queen Europe» - это европейский конкурс, целью которого является продвижение истинной красоты, женственности и благородства."}</p>
							<p>{this.props.lang === "Eng" ? "«Miss Queen Europe» is a grandiose and unique project." : "«Miss Queen Europe» - грандиозный и уникальный проект"}</p>
							<p>{this.props.lang === "Eng" ? "Representatives from more than 50 European countries take part in the «Miss Queen Europe» competition. To date, qualifying tours are held in more than 50 countries in Europe, more than 24,000 nominees will participate in castings." : "В конкурсе «Miss Queen Europe» принимают участие представители более чем 50 европейских стран. На сегодняшний день квалификационные туры проводятся в более чем 50 странах Европы, в кастингах будут участвовать более 24,000 номинанток."}</p>
							<p>{this.props.lang === "Eng" ? "The main focus of the competition «Miss Queen Europe» is the promotion of charitable and social initiatives and the development of cultural missions in European countries." : "Основным направлением конкурса «Miss Queen Europe» является продвижение благотворительных и социальных инициатив и развитие культурных миссий в европейских странах."}</p>
							<p>{this.props.lang === "Eng" ? "One of the main tasks of the all-European competition «Miss Queen Europe» is to identify and unite worthy representatives from 50 European countries to host GRAND FINAL - a grand European social event in Kyiv, Ukraine" : "Одна из главных задач общеевропейского конкурса «Miss Queen Europe» - выявить и объединить достойных представительниц из 50 европейских стран, чтобы провести GRAND FINAL - грандиозное европейское светское мероприятие в Киеве, Украина"}</p>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

export default About;