import React, {Fragment} from 'react';
import NewButtonApply from './button-apply/NewButtonApply';
import { Link } from 'react-router-dom';
import NewModal from "../NewHeadContainer/reg-modal/NewModal"

import logo from '../../../../logo_white.png'

import './headcontainer.css';

class NewHeadContainer extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isOpen: false
		}
		this.showModal = this.showModal.bind(this);
		this.updateData = this.updateData.bind(this);
	}

	showModal() {
		this.setState({
			isOpen: true
		})
	}
	updateData(value) {
		this.setState({ 
			isOpen: value 
		})
	 }

	render() {
		const flags = ['Finland', 'Sweden', 'Liechtenstein', 'Norway', 'Ireland','Denmark','Netherlands', 'San-Marino', 'Luxembourg', 'Belgium', 'Germany', 'Poland', "Liberland", "Andorra",
		'Switzerland', 'Austria', 'Hungary', 'Slovakia', 'Serbia', 'Macedonia', 'Ukraine','Lithuania', 'Latvia', 'Estonia', 'Romania', 'Bulgaria', 'Bosnia', 'Czech',
		'Cyprus', 'Monako', 'Greece', 'Albania', 'Montenegro', 'Croatia', 'Turkey', 'Tunisia','Malta', 'Italy', 'France', 'Marocco', 'Spain', 'Portugal', 'Azerbaijan',
		'Belarus', 'Moldova', 'Georgia', 'Kazakhstan', 'Russia', 'Israel', 'Armenia'];
		const artists = [
			{
				name: "Modjo",
				img: "modjo"
			},
			{
				name: "Sophie Ellis-Bextor",
				img: "ellie"
			},
			{
				name: "Haddaway",
				img: "haddaway"
			},
			{
				name: "Bosson",
				img: "bosson"
			}
		];
		return (
			<Fragment>
				<div className="head-container">
					{this.state.isOpen ? <NewModal isOpen={this.state.isOpen} updateData={this.updateData}/> : null}
					<div className="primary-header">
						<p>{this.props.lang === "Eng" ? "KYIV" : "КИЕВ"} <br /> 02.11.2019</p>
						<img src={logo} alt="big logo" />
						<p>{this.props.lang === "Eng" ? "INTERNATIONAL CONVENTIONAL CENTER PARKOVY" : "МЕЖДУНАРОДНЫЙ КОНФЕРЕНЦ-ЦЕНТР PARKOVY"}</p>
					</div>
					<div className="grand-final-title">
						<h1>{this.props.lang === "Eng" ? "Grand Final" : "Гранд Финал"}</h1>
					</div>

					<div className="grand-final-subtitle">
						{
							this.props.lang === "Eng" ?
								<div>
									<p>November 2, Kiev. Grand Final of the Miss Queen Europe 2019 European Beauty Contest</p>
									<p>Venue Congress Hall Park, Park Alley 16-A</p>
									<p>The event starts at 18.00 (the red carpet)</p>
									<p>Start of the show - 19.00 (live broadcast)</p>
									<p>Throughout the evening, online voting will be held in 50 countries and territories of Europe in a separate nomination, "Choice of Europe"</p>
									<p>The staging of the grand show was created by the best directors and choreographers in Europe</p>
									<p>The format of the show: Social events, black tie, gala dinner, shows and closed after-party</p>
									<p>Hosts of the show: Timur Miroshnichenko, Yana Krasnikova, Anton Polishchuk and Yulia Krauz</p>
									<p>Special guests: premium audience, jury members and guest stars of SOPHIE ELLIS-BEXTOR, BOSSON, HADDAWAY, MODJO, LAGANZA</p>
								</div>
								:
								<div>
									<p>2 ноября, Киев. Гранд Финал европейского конкурса красоты «Miss Queen Europe 2019»</p>
									<p>Место проведения: Конгресс-холл Парковый, Парковая аллея 16-А</p>
									<p>Старт мероприятия - 18.00 красная дорожка</p>
									<p>Старт шоу – 19.00 (прямой эфир)</p>
									<p>На протяжении всего вечера будет проходить онлайн голосование в 50-ти странах и территориях Европы в отдельной номинации «Выбор Европы»</p>
									<p>Постановка грандиозного шоу создана лучшими режиссёрами и хореографами Европы</p>
									<p>Формат шоу: Светский раут, black tie, гала ужин, шоу и закрытое after-party</p>
									<p>Ведущие шоу: Тимур Мирошниченко, Яна Красникова, Антон Полищук и Юлия Крауз</p>
									<p>Специальные гости: премиальная аудитория, члены жюри и приглашенные звёзды SOPHIE ELLIS-BEXTOR, BOSSON, HADDAWAY, MODJO, LAGANZA</p>
								</div>
						}
					</div>
					<div className="grand-countries">
						<div>
							{flags.map((item, id) => {
								if(id < 25) {
									let url = require('../../../more/map/flags/' + item.toLowerCase() + '.png');
									return <img src={url} alt="flag" key={id + 120} />;
								}
								else {
									return false;
								}
							})}
						</div>
							<p>50<br/>{this.props.lang === "Eng" ? "countries" : "стран"}</p>
						<div>
							{flags.map((item, id) => {
								if(id > 24) {
									let url = require('../../../more/map/flags/' + item.toLowerCase() + '.png');
									return <img src={url} alt="flag" key={id + 220} />;
								}
								else {
									return false;
								}
							})}
						</div>
					</div>
					<div className="artists">
						{artists.map((item,id) => {
							let url = require("./" + item.img.toLowerCase() + '.png');
							return (
								<div className="artists-item" key={300+id}>
									<img src={url} alt="artist"/>
									<p>{item.name}</p>
								</div>
							);
						})}
					</div>
					
					<div className="head-logo">
						<img src={logo} alt=""/>
					</div>
					<em className="head-copy">2018 &copy; Miss Queen Europe. All rights reserved.</em>
				</div>
				<div className="head-title"> 
					<div className="links-container">
						<button id="registration" className="registration" onClick={this.showModal} >{this.props.lang === "Eng" ? "Registration" : "Регистрация"}</button>
						<Link to={{pathname: '/more', lang: this.props.lang}}><NewButtonApply text={this.props.lang === "Eng" ? "More" : "Подробнее"}/></Link>
					</div>
					<h4 className="children-restrict">{this.props.lang === "Eng" ? "*The contest does not restrict the participation of nominees that have children." : "*Конкурс не ограничивает участия номинанток с наличием детей." }</h4>
				</div>
				</Fragment>
		);
	}
}

export default NewHeadContainer;
