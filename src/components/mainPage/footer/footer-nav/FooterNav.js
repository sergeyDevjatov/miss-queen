import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './footer-nav.css'

class FooterNav extends Component {
	render() {
		return (
			<div className="footer-nav">
				<ul className="footer-nav-list">
					<li className="footer-nav-item">
					{ this.props.main ? 
						<a href={this.props.main ? '#home' : '/'} className="footer-nav-link">{this.props.lang === "Eng" ? "Home" : "Главная"}</a> :
						<Link to={'/#home'}>{this.props.lang === "Eng" ? "Home" : "Главная"}</Link>
					}
					</li>
					<li className="footer-nav-item">
					{ this.props.main ? 
						<a href={this.props.main ? '#about' : '/'} className="footer-nav-link">{this.props.lang === "Eng" ? "About" : "О нас"}</a> :
						<Link to={'/#about'}>{this.props.lang === "Eng" ? "About" : "О нас"}</Link> 
					}
					</li>
					<li className="footer-nav-item">
					{ this.props.main ? 
						<a href={this.props.main ? '#goals' : '/'} className="footer-nav-link">{this.props.lang === "Eng" ? "Goals" : "Задачи"}</a> :
						<Link to={'/#goals'}>{this.props.lang === "Eng" ? "Goals" : "Задачи"}</Link>
					}
					</li>
					<li className="footer-nav-item">
						{ this.props.main ? 
							<a href={this.props.main ? '#services' : '/'} className="footer-nav-link">{this.props.lang === "Eng" ? "Services" : "Сервисы"}</a> :
							<Link to={'/#services'}>{this.props.lang === "Eng" ? "Services" : "Сервисы"}</Link>
						}
					</li>
					<li className="footer-nav-item">
						{ this.props.main ? 
							<a href={this.props.main ? '#parthners' : '/'} className="footer-nav-link">{this.props.lang === "Eng" ? "Partners" : "Партнеры"}</a> :
							<Link to={'/#parthners'}>{this.props.lang === "Eng" ? "Partners" : "Партнеры"}</Link>
						}
					</li>
				</ul>
			</div>
		);
  	}
}

export default FooterNav;