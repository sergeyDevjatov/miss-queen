import React, { Component } from 'react';
import './contacts.css'

class Contacts extends Component {
	render() {
		return (
			<div className='footer-contacts'>
				<ul className="footer-contacts-list">
					<li className="footer-contacts-item">
						<span>Phone:<a href="tel:0800503699"className="footer-contacts-link"> 0 800 50 36 99</a></span>
					</li>
					<li className="footer-contacts-item">
						<span>email:<a href="mailto:missqueeneurope@zoho.com" className="footer-contacts-link"> missqueeneurope@zoho.com</a></span>
					</li>
					<li className="footer-contacts-item">
						<span><a href="mailto:commerce@missqueeneurope.com" className="footer-contacts-link"> commerce@missqueeneurope.com</a></span>
					</li>
					<li className="footer-contacts-item">
						{/* <span>Press:<a href="/"className="footer-contacts-link"> press@dialoguekey.com</a></span> */}
					</li>
				</ul>
			</div>
		);
  	}
}

export default Contacts;
