import React, { Component } from 'react';
import './social.css'

class Social extends Component {
	render() {
		return (
			<div className='footer-social'>
				<a href="https://www.facebook.com/missqueeneurope2019" rel="noopener noreferrer" className="social-link">
					Facebook
				</a>
				<a href="https://inrating.top/u/MissQueenEurope" target="_blank" rel="noopener noreferrer" className="social-link">
					InRating
				</a>
				<a href="https://www.instagram.com/missqueeneurope" rel="noopener noreferrer" className="social-link">
					Instagram
				</a>
			</div>
		);
  	}
}

export default Social;
