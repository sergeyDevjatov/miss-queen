import React, { Component } from 'react';
import PackageApply from './packageApply/PackageApply'
import './packageeffect.css'

class PackageEffect extends Component {
	render() {
		const items = this.props.items;
		const style = this.props.dark ? "dark" : ""
		return (
			<div className="package-effect">
				<strong className={style}>{this.props.title}</strong>
				<ul className="package-list">
					{Object.keys(items).map(function(item, id) {
						return (items[item] ? 
							<li className="package-list-item" key={id}><span className="package-check"></span></li> : 
							<li className="package-list-item" key={id}><span className="package-discheck"></span></li>
						);
					})}
				</ul>
				<PackageApply/>
			</div>
		);
  	}
}

export default PackageEffect;
