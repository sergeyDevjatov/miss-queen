import React, { Component } from 'react';
import './packageapply.css'

class PackageApply extends Component {
	render() {
		return (
			<div className={`apply-block ${this.props.style}`}>
				<a href="/" className="apply-link">Apply now</a>
			</div>
		);
  	}
}

export default PackageApply;
