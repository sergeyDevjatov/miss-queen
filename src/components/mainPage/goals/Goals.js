import React, { Component } from 'react';
import girl from './girl_goals.png';
import './goals.css'

class Membership extends Component {
	render() {
		return (
			<div className="goals">
				<div className="goals-container">	
					<div className="goals-text">
						<h1>{this.props.lang === "Eng" ? "Goals" : "Задачи"}</h1>
						<p>{this.props.lang === "Eng" ? "The main task of the organizing committee of the competition «Miss Queen Europe» is to identify the most worthy representatives of their countries who will fight in the finals for the title of «Miss Queen Europe» and other premium titles, as well as carry out a charitable mission in the European countries throughout the year to secure a career professional and stellar growth" : "Главная задача организационного комитета конкурса «Miss Queen Europe» - выявить самых достойных представительниц своих стран, которые будут сражаться в финале за звание «Miss Queen Europe» и другие премиальные титулы, а также нести благотворительную миссию в страних Европы на протяжении года, кроме того, обеспечить себе карьерный профессиональный и звездный рост"}</p>
						<p>{this.props.lang === "Eng" ? "Tasks that implements the organizing committee of the contest «Miss Queen Europe»:" : "Задачи, которые реализовывает оргкомитет конкурса «Miss Queen Europe»:"}</p>
						<ul className="goals-list">
							<li>{this.props.lang === "Eng" ? "Popularization of the contest «Miss Queen Europe» among potential nominees, spectators, partners, public and charitable organizations" : "Популяризация конкурса «Miss Queen Europe» среди потенциальных номинанток, зрителей, партнеров, общественных и благотворительных организаций."}</li>
							<li>{this.props.lang === "Eng" ? "Make a selection of nominees, by transparent vote, which will represent their country in the final of the contest «Miss Queen Europe»" : "Осуществить выбор номинантки, путем прозрачного голосования, которая представит свою страну в финале конкурса «Miss Queen Europe»"}</li>
							<li>{this.props.lang === "Eng" ? "Make a choice, among the most worthy nominees, winners and holders of the title «Miss Queen Europe» as well as other premium titles during the grand final, by a transparent vote involving the star jury" : "Осуществить выбор, среди самых достойных номинанток, победительницы и обладательницы титула «Miss Queen Europe» а также других премиальных титулов во время проведения гранд финала, путем прозрачного голосования с привлечением звездного жюри "}</li>
							<li>{this.props.lang === "Eng" ? "Creating and conducting the final ceremony, the grand finale of «Miss Queen Europe», popularization of the European competition as the best project in the field of beauty and fashion industry." : "Создание и проведение финальной церемонии, гранд финала «Miss Queen Europe», популяризация общеевропейского конкурса, как лучшего проекта в области красоты и fashion индустрии."}</li>
							<li>{this.props.lang === "Eng" ? "Popularization of moral values, charity and commonwealth of European countries under the auspices of the European beauty contest." : "Популяризация нравственных ценностей, благотворительности и содружества европейских стран под эгидой проведения общеевропейского конкурса красоты."}</li>
							<li>{this.props.lang === "Eng" ? "The charitable mission of the winners of the grand finals in European countries in conjunction with major European charitable foundations" : "Благотворительная миссия победительниц гранд финала по европейским странам совмесно с крупными европейкими благотворительными фондами"}</li>
						</ul>
					</div>
					<div className="goals-img">
						<img src={girl} alt="goals-girl"/>
					</div>				
				</div>
			</div>
		);
  	}
}

export default Membership;
