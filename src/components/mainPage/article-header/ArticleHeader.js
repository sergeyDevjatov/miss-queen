import React, { Component } from 'react';
import './articleheader.css';

class AboutHeader extends Component {
	render() {
		return (
			<div className="about-header">
				<span>{this.props.title}</span>
			</div>
		);
	}
}

export default AboutHeader;