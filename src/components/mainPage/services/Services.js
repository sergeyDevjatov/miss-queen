import React, { Component } from 'react';
import girl from '../services/girl_services_bg.png';
import './services.css'

class Services extends Component {
	render() {
		return (
			<div className="services">
				<div className="services-content">
					<div className="services-img">
						<img src={girl} alt="services-girl"/>
					</div>	
					<div className="services-text">
						<h1>{this.props.lang === "Eng" ? "Services" : "Сервисы"}</h1>
						<p>{this.props.lang === "Eng" ? "Our team is specialists who are in love with their work." : "Наша команда - это специалисты которые влюблены в свою работу."}</p>
						<p>{this.props.lang === "Eng" ? "«Miss Queen Europe» team are European-class specialists who have vast experience in the beauty and fashion industry. We are stands for an individual approach, so each questionnaire is considered by us individually." : "Команда «Miss Queen Europe» это специалисты европейского класса, которые имеют огромный опыт в индустрии красоты и моды. Мы за индивидуальный подход, поэтому каждая анкета рассматривается нами в индивидуальном порядке. "}</p>
						<p>{this.props.lang === "Eng" ? "Every day we review dozens of questionnaires and various offers of cooperation, we are always open for communication and dialogue." : "Ежедневно мы рассматриваем десятки анкет и различные предложения о сотрудничестве, мы всегда открыты для общения и диалога. "}</p>
						<p>{this.props.lang === "Eng" ? "Our team also provides services for the support and organization of events in the beauty industry and fashion shows. We organize any event, anywhere in the world!" : "Наша команда также оказывает услуги по сопровождению и организации мероприятий в индустрии красоты и фэшн-шоу. Мы организуем любое мероприятие, в любой точке мире!"}</p>
						<div>
							<p>{this.props.lang === "Eng" ? "What we do?" : "Чем мы занимаемся"}</p>
							<ul>
								<li>{this.props.lang === "Eng" ? "selections & castings, any level in any country" : "отборы и кастинги, любого уровня в любой стране"}</li>
								<li>{this.props.lang === "Eng" ? "E-Voting of any subject in any country" : "E-Voting голосование любого типа в любой стране"}</li>
								<li>{this.props.lang === "Eng" ? "organizing and conducting competitions of any level and anywhere in the world" : "организация и проведение общественных благотворительных и fashion мероприятий любого уровня в любой точке мира"}</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		);
  	}
}

export default Services;
