import React, { Component } from 'react';
import './arrowlink.css'

class ArrowLink extends Component {
	render() {
		return (
			<div className="join">
				<a href={ this.props.to } className="join-link">
					{this.props.children}
				</a>
			</div>
		);
  	}
}

export default ArrowLink;
