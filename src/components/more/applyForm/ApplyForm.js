import React from 'react';
import Input from './input/Input';
import SubmitForm from './submitForm/SubmitForm';
import logo from '../../../logo_pink.png';
import DatePicker from 'react-date-picker';

import './apply-form.css';

import './../../../../node_modules/bootstrap/dist/css/bootstrap.min.css'

class ApplyForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			firstname: '',
			lastname: '',
			country: '',
			birth: new Date(),
			height: '',
			weight: '',
			params: '',
			insta: '',
			mail: '',
			phone: ''
		}
		this.handleFormCheck = this.handleFormCheck.bind(this);
		this.changeDate = this.changeDate.bind(this);
	}
	handleFormCheck(input) {
		const name = input.name;
		this.setState({
			[name]: input.value
		});
	}
	handleChangeCountry = (selectedOption) => {
		this.setState({ country: selectedOption.value });
	}

	changeDate = date => this.setState({ 
		birth: date
	 })

	render() {
		const options = [
			{ value: 'Albania', label: 'Albania' },
			{ value: 'Andorra', label: 'Andorra' },
			{ value: 'Armenia', label: 'Armenia' },
			{ value: 'Austria', label: 'Austria' },
			{ value: 'Azerbaijan', label: 'Azerbaijan' },
			{ value: 'Belarus', label: 'Belarus' },
			{ value: 'Belgium', label: 'Belgium' },
			{ value: 'Bosnia', label: 'Bosnia' },
			{ value: 'Bulgaria', label: 'Bulgaria' },
			{ value: 'Croatia', label: 'Croatia' },
			{ value: 'Cyprus', label: 'Cyprus' },
			{ value: 'Czech', label: 'Czech' },
			{ value: 'Denmark', label: 'Denmark' },
			{ value: 'Estonia', label: 'Estonia' },
			{ value: 'Finland', label: 'Finland' },
			{ value: 'France', label: 'France' },
			{ value: 'Georgia', label: 'Georgia' },
			{ value: 'Germany', label: 'Germany' },
			{ value: 'Greece', label: 'Greece' },
			{ value: 'Hungary', label: 'Hungary' },
			{ value: 'Ireland', label: 'Ireland' },
			{ value: 'Israel', label: 'Israel' },
			{ value: 'Italy', label: 'Italy' },
			{ value: 'Kazakhstan', label: 'Kazakhstan' },
			{ value: 'Latvia', label: 'Latvia' },
			{ value: 'Liberland', label: 'Liberland' },
			{ value: 'Liechtenstein', label: 'Liechtenstein' },
			{ value: 'Lithuania', label: 'Lithuania' },
			{ value: 'Luxembourg', label: 'Luxembourg' },
			{ value: 'Macedonia', label: 'Macedonia' },
			{ value: 'Malta', label: 'Malta' },
			{ value: 'Moldova', label: 'Moldova' },
			{ value: 'Monaco', label: 'Monaco' },
			{ value: 'Montenegro', label: 'Montenegro' },
			{ value: 'Morocco', label: 'Morocco' },
			{ value: 'Netherlands', label: 'Netherlands' },
			{ value: 'Norway', label: 'Norway' },
			{ value: 'Poland', label: 'Poland' },
			{ value: 'Portugal', label: 'Portugal' },
			{ value: 'Romania', label: 'Romania' },
			{ value: 'Russia', label: 'Russia' },
			{ value: 'San-Marino', label: 'San-Marino' },
			{ value: 'Serbia', label: 'Serbia' },
			{ value: 'Slovakia', label: 'Slovakia' },
			{ value: 'Spain', label: 'Spain' },
			{ value: 'Sweden', label: 'Sweden' },
			{ value: 'Switzerland', label: 'Switzerland' },
			{ value: 'Tunisia', label: 'Tunisia' },
			{ value: 'Turkey', label: 'Turkey' },
			{ value: 'Ukraine', label: 'Ukraine' },
		];
		return (
			<div className="form" id="regForm">
				<form className='container' method='POST' onSubmit={this.handleFormCheck}
					action='https://script.google.com/macros/s/AKfycbxns5O82iSVV6gjigiJ4WO2t2Qn9ZfJh4aUIDYzdjekHmbjoOCu/exec'>
					<div className="form-logo">
						<img src={logo} alt="logo"/>
					</div>
					<h2 className="form-title">{this.props.lang === "Eng" ? "Registration form" : "Форма регистрации"}</h2>
					<h4>{this.props.lang === "Eng" ? "The contest does not restrict the participation of nominees that have children." : "Конкурс не ограничивает участия номинанток с наличием детей."}</h4>
					<div className="form-names">
						<Input type = {'text'}
							title = {this.props.lang === "Eng" ? "First Name" : "Имя"} 
							name = {'firstname'}
							value = {this.state.firstname} 
							placeholder = {this.props.lang === "Eng" ? "first name" : "имя"}
							handleInput = {this.handleFormCheck}
						/>
						<Input type = {'text'}
							title = {this.props.lang === "Eng" ? "Last Name" : "Фамилия"} 
							name = {'lastname'}
							value = {this.state.lastname} 
							placeholder = {this.props.lang === "Eng" ? "last name" : "фамилия"} 
							handleInput = {this.handleFormCheck}
						/>
					</div>
					<div className="form-group">
						<label className='form-label'>{this.props.lang === "Eng" ? "Select the country you will represent" : "Cтрана которую представляете"} </label> <br></br>
						<select name = {'country'} handleInput = {this.handleFormCheck} className="select-country">
							{options.map(function(item, index) {
								return (
									<option value={item.value} key={index}>{item.label}</option>
								);
							})}
						</select>
					</div>
					<div className="form-group">
						<label className='form-label'>{this.props.lang === "Eng" ? "Select your date of birth" : "Дата Вашего рождения"}</label>
						<br/>
						<DatePicker
							onChange={this.changeDate}
							value={this.state.birth}
							minDetail="decade"
							required
        				/>
					</div>
					<div className='form-params'>
						<Input type = {'number'}
							title = {this.props.lang === "Eng" ? "Height" : "Рост"}
							name = {'height'}
							value = {this.state.height} 
							placeholder = {this.props.lang === "Eng" ? "height" : "рост"}
							handleInput = {this.handleFormCheck}
						/>
						<Input type = {'number'}
							title = {this.props.lang === "Eng" ? "Weight" : "Вес"} 
							name = {'weight'}
							value = {this.state.weight} 
							placeholder = {this.props.lang === "Eng" ? "weight" : "вес"}
							handleInput = {this.handleFormCheck}
						/>
						<Input type = {'text'}
							title = {this.props.lang === "Eng" ? "Parameters (90/60/90)" : "Параметры (90/60/90)"}
							name = {'params'}
							value = {this.state.params} 
							placeholder = {this.props.lang === "Eng" ? "parameters" : "параметры"}
							handleInput = {this.handleFormCheck}
						/>
					</div>
					<Input type = {'text'}
							title = {this.props.lang === "Eng" ? "Instagram profile" : "Instagram профиль"} 
							name = {'insta'}
							value = {this.state.insta} 
							placeholder = {this.props.lang === "Eng" ? "Instagram profile" : "Instagram профиль"}
							handleInput = {this.handleFormCheck}
					/>
					<Input type = {'email'}
							title = {this.props.lang === "Eng" ? "Email" : "Email"}
							name = {'mail'}
							value = {this.state.mail} 
							placeholder = {this.props.lang === "Eng" ? "email" : "email"}
							handleInput = {this.handleFormCheck}
					/>
					<Input type = {'tel'}
							title = {this.props.lang === "Eng" ? "Phone (including country code)" : "Ваш номер телефона (с кодом страны)"}
							name = {'phone'}
							value = {this.state.phone} 
							placeholder = {this.props.lang === "Eng" ? "+xx(xxx)xxxxxxx" : "+xx(xxx)xxxxxxx"}
							handleInput = {this.handleFormCheck}
					/>
					<SubmitForm data={this.state} lang = {this.props.lang}/>
				</form>
			</div>
		);
	}
}

export default ApplyForm;

