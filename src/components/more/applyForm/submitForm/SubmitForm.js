import React from 'react';

import './submit-form.css';

class SubmitForm extends React.Component {
	render() {
		return (
			<div className="form-submit">
				<input className="btn btn-primary" type='submit' value={this.props.lang === "Eng" ? "Send" : "Отправить"} onClick={this.submitForm}/>
			</div>
		);
	}
}

export default SubmitForm;