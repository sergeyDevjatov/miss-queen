import React from 'react';
import SideBar from '../mainPage/head/SideBar/SideBar';
import ApplyForm from './applyForm/ApplyForm';
import Logo from './logo/Logo';
import Map from './map/Map';
import MoreHead from './more-head/MoreHead';
import Footer from '../mainPage/footer/Footer';
import './more.css';

class More extends React.Component {
	constructor(props) {
		super(props);
		this.changeLang = this.changeLang.bind(this);
	}
	changeLang(e) {
		e.preventDefault();
		localStorage.setItem("language", e.target.innerText)
		this.setState({
			lang: e.target.innerText
		})
	}
	render() {
		const lang = localStorage.getItem("language")
		return (
			<div id='more'>
				<SideBar lang={lang}/>
				<div className="more-lang">
					<a href="/" className={localStorage.getItem('language') === "Rus" ? "active" : "" } onClick={this.changeLang}>Rus</a>
					<a href="/" className={localStorage.getItem('language') === "Eng" ? "active" : "" } onClick={this.changeLang}>Eng</a>
				</div>
				<MoreHead lang={lang}/>
				<Map lang={lang}/>
				<div className="title">
					<h1>GRAND FINAL <br/>Miss Queen Europe 2019</h1>
					<h2>Kyiv</h2>
				</div>
				<Logo />
				<ApplyForm lang={lang}/>
				<Footer lang={lang}/>
			</div>
		);
	}
}

export default More;

