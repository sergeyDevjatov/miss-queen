import React from 'react';
import "./loading.css";
import loading from "./loading.gif";

class Loading extends React.Component {
	render() {
		return (
			<div className="loading">
				<img src={loading} alt="loading"/>
			</div>
		);
	}
}

export default Loading;