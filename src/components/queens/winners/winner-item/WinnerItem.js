import React, { Component } from 'react';
import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button'
import './winner-item.css';

class WinerItem extends Component {
	render() {
		return(
			<Card>
				<Card.Header>
				<Accordion.Toggle as={Button} variant="link" eventKey={this.props.id}>
					<div>
						<img src={this.props.flag} alt={this.props.countryEn} />
						{this.props.lang === 'Eng' ? this.props.countryEn : this.props.countryRus }
					</div>
				</Accordion.Toggle>
				</Card.Header>
				{/* <Accordion.Collapse eventKey={this.props.id}> */}
				<Card.Body>
					<div className="winner-row">
					{this.props.imgs.map((img, id) => {
						return(
							<div key={id}>
								<img src={img} alt={this.props.countryEn + id} key={id+100}/>
								<h3>{this.props.winners[id]}</h3>
							</div>
						);
					})}
					</div>
				</Card.Body>
				{/* </Accordion.Collapse> */}
			</Card>
		);
	}
}

export default WinerItem;