import React, { Component } from 'react';
import WinnerItem from './winner-item/WinnerItem';
import Accordion from 'react-bootstrap/Accordion';
import './winners.css';

let winners = [
	{
		winners: ["Adelina Cani", "Geraldina Metani", "Eva Mertiri"],
		flag: 'albania',
		countryEn: 'Albania',
		countryRus: 'Албания',
		images: ["https://media.inrating.top/img/5TW1hpJaBnKEXQYC3f4M.jpg", 
			"https://media.inrating.top/img/01en17OSegsrAvKsAwat.jpg",
			"https://media.inrating.top/img/FQe7lfZfv9EfxhW0hkbX.jpg"
		]
	},
	{
		winners: ["Alba Martos", "Tania Iglesias", "Alba Sinfreu"],
		flag: 'andorra',
		countryEn: 'Andorra',
		countryRus: 'Андорра',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Andorra1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Andorra2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Andorra3.jpg"
		]
	},
	{
		winners: ["Narine Pluzyan", "Elen Hovsepyan", "Anjela Mkrtchyan"],
		flag: 'armenia',
		countryEn: 'Armenia',
		countryRus: 'Армения',
		images: ["https://media.inrating.top/img/tD9gq1ClDcFfhX0ECKQy.jpg",  
			"https://media.inrating.top/img/gkdIbIGV6sepa7lDRvVJ.jpg", 
			"https://media.inrating.top/img/iVtLL2PdZefjZG3gBvyf.jpg"
		]
	},
	{
		winners: ["Sabrina Tengler", "Larisa Popluca", "Drilona Zenelaj"],
		flag: 'austria',
		countryEn: 'Austria',
		countryRus: 'Австрия',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Austria1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Austria2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Austria3.jpg"
		]
	},
	{
		winners: ["Margarita Stepanova", "Hulya Bashirli", "Mara Jafarova"],
		flag: 'azerbaijan',
		countryEn: 'Azerbaijan',
		countryRus: 'Азербайджан',
		images: ["https://media.inrating.top/img/Fwxf7m6Ok3WCFqM02Dtf.jpg", 
			"https://media.inrating.top/img/DHM1AC10P06jWU8M8PLv.jpg",
			"https://media.inrating.top/img/656ZlbI0RLEAZzrPjE33.jpg"
		]
	},
	{
		winners: ["Darya Duben", "Aliaksandra Tarasevich", "Anastasiya Savich"],
		flag: 'belarus',
		countryEn: 'Belarus',
		countryRus: 'Белоруссия',
		images: ["https://media.inrating.top/img/uEU0miRvHBtHDKjnlbxN.jpg", 
			"https://media.inrating.top/img/QK7zRrye3v46z1x1HWk7.jpg",
			"https://media.inrating.top/img/YjxjJ0y9HvaH8BHLyOji.jpg"
		]
	},
	{
		winners: ["Barbara Winand", "Barbara Winand", "Elezabeth Aleksandrova"],
		flag: 'belgium',
		countryEn: 'Belgium',
		countryRus: 'Бельгия',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Belgium1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Belgium2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Belgium3.jpg"
		]
	},
	{
		winners: ["Nensi Dusper", "Jelena Jovanivic", "Amina Selimovic"],
		flag: 'bosnia',
		countryEn: 'Bosnia',
		countryRus: 'Босния',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Bosnia1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Bosnia2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Bosnia3.jpg"
		]
	},
	{
		winners: ["Blagovesta Damyan", "Katerina Kelesh", "Mira Simeonova"],
		flag: 'bulgaria',
		countryEn: 'Bulgaria',
		countryRus: 'Болгария',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Bulgaria1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Bulgaria2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Bulgaria3.jpg"
		]
	},
	{
		winners: ["Nensi Gruicic", "Ivana Kaleb", "Maria Ivanova "],
		flag: 'croatia',
		countryEn: 'Croatia',
		countryRus: 'Хорватия',
		images: ["https://media.inrating.top/img/H1OwYaLKk6vbnTRY1rhB.jpg", 
			"https://media.inrating.top/img/oO73fO5nNJu2am4JPwbX.jpg",
			"https://media.inrating.top/img/vWO3YxhCSyDvYqnNgcxD.jpg"
		]
	},
	{
		winners: ["Ellada Ismailova", "Marta Buczuk", "Vlada Kiraner"],
		flag: 'cyprus',
		countryEn: 'Cyprus',
		countryRus: 'Кипр',
		images: ["https://media.inrating.top/img/be2DCPZFOAmWG37Q33cY.jpg", 
			"https://media.inrating.top/img/dtbTof6gR2fPubFyYFC6.jpg",
			"https://media.inrating.top/img/R3z8EquSiTJA7AuCfMDz.jpg"
		]
	},
	{
		winners: ["Martina Beresova", "Nicole Sachova", "Pavlina Ryskova"],
		flag: 'czech',
		countryEn: 'Czech Republic',
		countryRus: 'Чехия',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Czech1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Czech2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Czech3.jpg"
		]
	},
	{
		winners: ["Cecilie Dissing", "Reem Odeh", "Moneca Latif"],
		flag: 'denmark',
		countryEn: 'Denmark',
		countryRus: 'Дания',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Denmark1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Denmark2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Denmark3.jpg"
		]
	},
	{
		winners: ["Karina Demutskaja", "Liisi Tammoja", "Vitaliia Pus"],
		flag: 'estonia',
		countryEn: 'Estonia',
		countryRus: 'Естония',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Estonia1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Estonia2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Estonia3.jpg"
		]
	},
	{
		winners: ["Katarina Juselius", "Lenita Susi", "Emilia Lepomaki"],
		flag: 'finland',
		countryEn: 'Finland',
		countryRus: 'Финляндия',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Finland1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Finland2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Finland3.jpg"
		]
	},
	{
		winners: ["Ksenia Latti", "Anastasia Studenikina", "Deborah Michanol"],
		flag: 'france',
		countryEn: 'France',
		countryRus: 'Франция',
		images: ["https://media.inrating.top/img/mhxadfxGO8u9mj8nWtjA.jpg", 
			"https://media.inrating.top/img/3Vao0oOWlWit4F3jmfru.jpg",
			"https://media.inrating.top/img/DY4mNuBqPO1KioaJLwpK.jpg"
		]
	},
	{
		winners: ["Chanturiya Eka", "Juliana Zhvania", "Anastasia Danelia"],
		flag: 'georgia',
		countryEn: 'Georgia',
		countryRus: 'Грузия',
		images: ["https://media.inrating.top/img/oG0VA8YbLAN2iLTr4DHf.jpg", 
			"https://media.inrating.top/img/8eAJbF0dMOb78TbFIV8V.jpg",
			"https://media.inrating.top/img/wZw8UGlDROd25DwAB7z2.jpg"
		]
	},
	{
		winners: ["Weronika Weilgus", "Pia Baark", "Julia Luczynska"],
		flag: 'germany',
		countryEn: 'Germany',
		countryRus: 'Германия',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Germany1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Germany2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Germany3.jpg"
		]
	},
	{
		winners: ["Foteini Barnasa", "Elizaveta Litvinova", "Tetiana Solomka"],
		flag: 'greece',
		countryEn: 'Greece',
		countryRus: 'Греция',
		images: ["https://media.inrating.top/img/YCa76DISWUi9h22m0LTL.jpg", 
			"https://media.inrating.top/img/wfSVXcqTxzHYptF7zRfi.jpg",
			"https://media.inrating.top/img/28s1VQFhMK7K5uGezP4d.jpg"
		]
	},
	{
		winners: ["Dina Tkachyova", "Alexandra Jankovics", "Laura Gregusova"],
		flag: 'hungary',
		countryEn: 'Hungary',
		countryRus: 'Венгрия',
		images: ["https://media.inrating.top/img/Boik9Y59e3kmbz6cwOb7.jpg", 
			"https://media.inrating.top/img/jWRxEpoS5uZzV55u0TrH.jpg",
			"https://media.inrating.top/img/qxuP4v3fDpYFd1epMmqM.jpg"
		]
	},
	{
		winners: ["Nika Rogozina", "Evelina Mikneviciute", "Victoria Chornomaz"],
		flag: 'ireland',
		countryEn: 'Ireland',
		countryRus: 'Ирландия',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Ireland1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Ireland2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Ireland3.jpg"
		]
	},
	{
		winners: ["Shoval-Hila Labin", "Aleksandra Kotliar", "Yana Muraveiko"],
		flag: 'israel',
		countryEn: 'Israel',
		countryRus: 'Израиль',
		images: ["https://media.inrating.top/img/ywVXeFxAdOcCHw9myCTG.jpg",  
			"https://media.inrating.top/img/FYz4x9JgoIfOk9ljU3hj.jpg", 
			"https://media.inrating.top/img/ayuNsnREewzwILe5PGJT.jpg"
		]
	},
	{
		winners: ["Larisa Bastanova", "Ilona Verchuk", "Natalia Spitkovska"],
		flag: 'italy',
		countryEn: 'Italy',
		countryRus: 'Италия',
		images: ["https://media.inrating.top/img/sefv5drVujNlZ1V2FvfR.jpg", 
			"https://media.inrating.top/img/1mR0bj6tdm6VlHVf9Doi.jpg",
			"https://media.inrating.top/img/F7QLLYfGoeDdXtDYc9Yl.jpg"
		]
	},
	{
		winners: ["Мухтар Лаура", "Aglen Tleulessova", "Assem Zhakish"],
		flag: 'kazakhstan',
		countryEn: 'Kazakhstan',
		countryRus: 'Казахстан',
		images: ["https://media.inrating.top/img/ITt2vPuMQzwdBRVm0Y2D.jpg", 
			"https://media.inrating.top/img/3rVvGpjevkaRogR3GYEd.jpg",
			"https://media.inrating.top/img/Af4UhDYF62GgOi9UbXjp.jpg"
		]
	},
	{
		winners: ["Ksenija Laskina", " Liene Leitane", "Karina Zatonska"],
		flag: 'latvia',
		countryEn: 'Latvia',
		countryRus: 'Латвия',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Latvia1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Latvia2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Latvia3.jpg"
		]
	},
	{
		winners: ["Jelena Ilic", "Kristina Karan", "Jovana Jovanovich"],
		flag: 'liberland',
		countryEn: 'Liberland',
		countryRus: 'Либерленд',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Liberland1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Liberland2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Liberland3.jpg"
		]
	},
	{
		winners: ["Alissia Lampert", "Sandy Ashley", "Melissa Tezanou"],
		flag: 'liechtenstein',
		countryEn: 'Liechtenstein',
		countryRus: 'Лихтенштейн',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Liechtenstein1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Liechtenstein2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Liechtenstein3.jpg"
		]
	},
	{
		winners: ["Valerija Gagarina ", "Rugile Ruksenaite", "Emilija Budginaite"],
		flag: 'lithuania',
		countryEn: 'Lithuania',
		countryRus: 'Литва',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Lithuania1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Lithuania2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Lithuania3.jpg"
		]
	},
	{
		winners: ["Emilija Budginaite", "Taiana Neto", "Selenay Alan"],
		flag: 'luxembourg',
		countryEn: 'Luxembourg',
		countryRus: 'Люксембург',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Luxembourg1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Luxembourg2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Luxembourg3.jpg"
		]
	},
	{
		winners: ["Angela Jakimovcka", "Ilirjana Saliu", "Nazlije Bajrami"],
		flag: 'macedonia',
		countryEn: 'Macedonia',
		countryRus: 'Македония',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Macedonia1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Macedonia2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Macedonia3.jpg"
		]
	},
	{
		winners: ["Nicole Sachova", "Zelenina Cristina", "Viktorii Viktorivska"],
		flag: 'malta',
		countryEn: 'Malta',
		countryRus: 'Мальта',
		images: ["https://media.inrating.top/img/tHMzUcVf8cUbbteyje2x.jpg", 
			"https://media.inrating.top/img/4xqkZRYn9AOOziO2cn5P.jpg",
			"https://media.inrating.top/img/XcyPbHGuN5kpxo00gWhP.jpg"
		]
	},
	{
		winners: ["Sofia-Margarita Deaghileva", "Cristyna Bereg", "Mihalachi Anna"],
		flag: 'moldova',
		countryEn: 'Moldova',
		countryRus: 'Молдавия',
		images: ["https://media.inrating.top/img/hBnpDS43NoeZ0nqHm5b5.jpg", 
			"https://media.inrating.top/img/AyK2E3jBNy8jGiepn7Gk.jpg",
			"https://media.inrating.top/img/yQAQwmh6ZvYuNqToxgbP.jpg"
		]
	},
	{
		winners: ["Olga Korol"],
		flag: 'monako',
		countryEn: 'Monaco',
		countryRus: 'Монако',
		images: ["https://media.inrating.top/img/yMbOloT5HNS6n5P33Zj2.jpg"
		]
	},
	{
		winners: ["Yasmine Oudaha", "Nihed Amzil", "Melis Yolay"],
		flag: 'marocco',
		countryEn: 'Morocco',
		countryRus: 'Марокко',
		images: ["https://media.inrating.top/img/Q2AI1Sl5aKBST2q9hrHm.jpg", 
			"https://media.inrating.top/img/W2cD0oHoVMxs5770Rk4Y.jpg",
			"https://media.inrating.top/img/wfup42RYYyEP59LS7yFO.jpg"
		]
	},
	{
		winners: ["Arma Skenderovic ", "Nevena Plamenac", "Amina Markovic"],
		flag: 'montenegro',
		countryEn: 'Montenegro',
		countryRus: 'Черногория',
		images: ["https://media.inrating.top/img/sYksRvLe9Y6o87HHhUgn.jpg", 
			"https://media.inrating.top/img/mu6TtdM67C5iVKMkV75S.jpg",
			"https://media.inrating.top/img/32tgoag21R3yjpPxkpPk.jpg"
		]
	},
	{
		winners: ["Ilse Helena Van Der Haar", "Ina Orlova", "Rayshree Debi Tewari"],
		flag: 'netherlands',
		countryEn: 'Netherlands',
		countryRus: 'Нидерланды',
		images: ["https://media.inrating.top/img/uW0QuGPXWzvomA4sxFDx.jpg", 
			"https://media.inrating.top/img/93BLpIxApzmtcNvglzrR.jpg",
			"https://media.inrating.top/img/pZvCEL41eTkI9vAnCqqN.jpg"
		]
	},
	{
		winners: ["Skaiste Virkovaite", " Sidra Hassan", "Nora Emilie"],
		flag: 'norway',
		countryEn: 'Norway',
		countryRus: 'Норвегия',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Norway1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Norway2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Norway3.jpg"
		]
	},
	{
		winners: ["Iryna Shevchenko", "Kamila Zlosnik", "Kamila Zlosnik"],
		flag: 'poland',
		countryEn: 'Poland',
		countryRus: 'Польша',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Poland1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Poland2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Poland3.jpg"
		]
	},
	{
		winners: ["Alexsandra Livitchi", "Mihaela Petcu", "Isabel Carvalho"],
		flag: 'portugal',
		countryEn: 'Portugal',
		countryRus: 'Португалия',
		images: ["https://media.inrating.top/img/DFLOQzqzJazc1D4ctn2W.jpg", 
			"https://media.inrating.top/img/waR7nzGLQfTAkKEDl0Ab.jpg",
			"https://media.inrating.top/img/8xB9EdsnEg0ciQVbVirf.jpg"
		]
	},
	{
		winners: ["Crina Barbu", "Eliza Iohana", "Alexandra Neacsa"],
		flag: 'romania',
		countryEn: 'Romania',
		countryRus: 'Румуния',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Romania1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Romania2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Romania3.jpg"
		]
	},
	{
		winners: ["Rosina Pit", "Наталья Антипина", "Lidia Cebotari"],
		flag: 'russia',
		countryEn: 'Russia',
		countryRus: 'Россия',
		images: ["https://media.inrating.top/img/bV6IzZhsX0ilXWCTrIKp.jpg",  
			"https://media.inrating.top/img/3IMjXvhD9LsH5pIOr5qJ.jpg", 
			"https://media.inrating.top/img/7f6f557b1sPvrq6YcA9y.jpg"
		]
	},
	{
		winners: ["Анастасия Крыгина"],
		flag: 'san-marino',
		countryEn: 'San Marino',
		countryRus: 'Сан Марино',
		images: ["https://media.inrating.top/img/ZiYqFDVjb7ktES8kaO63.jpg"
		]
	},
	{
		winners: ["Jovana Mihajlovic", "Anja Ivanovic", "Olivera Ramovic"],
		flag: 'serbia',
		countryEn: 'Serbia',
		countryRus: 'Сербия',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Serbia1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Serbia2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Serbia3.jpg"
		]
	},
	{
		winners: ["Kristina Ondrisikova", "Marinella Iordan", "Eulalia Malecova"],
		flag: 'slovakia',
		countryEn: 'Slovakia',
		countryRus: 'Словакия',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Slovakia1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Slovakia2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Slovakia3.jpg"
		]
	},
	{
		winners: ["Juls Muratet", "Sara Robles-Cervera", "Sofia Rubio"],
		flag: 'spain',
		countryEn: 'Spain',
		countryRus: 'Испания',
		images: ["https://media.inrating.top/img/MdU3qnWZLztEDmojimcu.jpg", 
			"https://media.inrating.top/img/3p5vcerMLLK3W885B0Ab.jpg",
			"https://media.inrating.top/img/JTk0zJSul2naRmHL01QO.jpg"
		]
	},
	{
		winners: ["Aida Selimi", "Kryshna Mae", "Agnes Samuel"],
		flag: 'sweden',
		countryEn: 'Sweden',
		countryRus: 'Швеция',
		images: ["https://media.inrating.top/img/iLvkyr3JRkMZMntaL8qq.jpg", 
			"https://media.inrating.top/img/qx5DgxHUzzDiQSJVTOaf.jpg",
			"https://media.inrating.top/img/LBMMQxeZyEOkPEiA8WtI.jpg"
		]
	},
	{
		winners: ["Marijana Danq", "Tatiana de Oliveira", "Luna Tzontlimazti"],
		flag: 'switzerland',
		countryEn: 'Switzerland',
		countryRus: 'Швейцария',
		images: ["https://media.inrating.top/storage/img/missqueeneurope/Switzerland1.jpg", 
		"https://media.inrating.top/storage/img/missqueeneurope/Switzerland2.jpg",
		 "https://media.inrating.top/storage/img/missqueeneurope/Switzerland3.jpg"
		]
	},
	{
		winners: ["Dhekra Goutali", "Ines Bennoomen", "Syrine Trabelsi"],
		flag: 'tunisia',
		countryEn: 'Tunisia',
		countryRus: 'Тунис',
		images: ["https://media.inrating.top/img/aH0dfYxC1s3kHSMp1xTj.jpg", 
			"https://media.inrating.top/img/MwuaVrhxSZ1MHMl1k8j3.jpg",
			"https://media.inrating.top/img/wlfHOFsvjdC3pFFyqRNu.jpg"
		]
	},
	{
		winners: ["Ilknur Yolay", "Pinar Baycinar", "Pelin Sikora"],
		flag: 'turkey',
		countryEn: 'Turkey',
		countryRus: 'Турция',
		images: ["https://media.inrating.top/img/0jlntVLPQfeAq6bF0QBm.jpg", 
			"https://media.inrating.top/img/RP9Hwn87C8H9aykaY6Nm.jpg",
			"https://media.inrating.top/img/12C7wzDePoikzOx8TkwR.jpg"
		]
	},
	{
		winners: ["Alexandra Skoryna", "Svitlana Khomenko", "Ivana Kiba"],
		flag: 'ukraine',
		countryEn: 'Ukraine',
		countryRus: 'Украина',
		images: ["https://media.inrating.top/img/RbMzSWx9gvp9Ce2Ib4wB.jpg", 
			"https://media.inrating.top/img/UwlywIoUu3biPBoiAKk4.jpg",
			"https://media.inrating.top/img/OnU39nkKtnjPE4GPjN1s.jpg"
		],
	}	
];

class Winners extends Component {
	render() {
		return (
			<div className="winners-container">
				<h2 className="winners-title">{this.props.lang === "Eng" ? "Winners" : "Победительницы"}</h2>
				<Accordion defaultActiveKey="0">
					{winners.map((item, id) => {
						let url = require('../../more/map/flags/' + item.flag.toLowerCase() + '.png');
						return (
							<WinnerItem countryEn={item.countryEn} countryRus={item.countryRus} winners={item.winners} imgs={item.images} flag={url} key={id} id={id} lang={this.props.lang} />
						);
					})}
				</Accordion>
			</div>
		);
	}
}

export default Winners;