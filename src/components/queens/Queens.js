import React from 'react';
import SideBar from '../mainPage/head/SideBar/SideBar';
import Header from './header/Header';
import Logo from '../more/logo/Logo';
import Winners from './winners/Winners';
import Footer from '../mainPage/footer/Footer';
import './queens.css';

class Queens extends React.Component {
	constructor(props) {
		super(props);
		this.changeLang = this.changeLang.bind(this);
	}
	changeLang(e) {
		e.preventDefault();
		localStorage.setItem("language", e.target.innerText)
		this.setState({
			lang: e.target.innerText
		})
	}
	render() {
		const lang = localStorage.getItem("language")
		return (
			<div id='queens'>
				<SideBar lang={lang}/>
				<Header lang={lang}/>
				<Logo />
				<div className="more-lang">
					<a href="/" className={localStorage.getItem('language') === "Rus" ? "active" : "" } onClick={this.changeLang}>Rus</a>
					<a href="/" className={localStorage.getItem('language') === "Eng" ? "active" : "" } onClick={this.changeLang}>Eng</a>
				</div>
				<Winners lang={lang} />
				<Footer lang={lang}/>
			</div>
		);
	}
}

export default Queens;
