import React from 'react';
import Logo from '../../../logo_pink.png';
import './header.css';

class Header extends React.Component {
	render() {
		return (
			<div id='queens-head'>
				<div className="more-logo">
					<img src={Logo} alt="logo" />
				</div>
				<p>{this.props.lang === "Eng" ? "According to the results of online voting in 50 countries and territories of Europe, 150 finalists became winners. These are the most worthy and charismatic beauties who will represent their country in the final of a grand show!" : "По результатам онлайн голосований, которые прошли в 50 странах и территориях Европы победителями стали 150 финалисток. Это самые достойные и харизматичные девушки, которые представят свою страну во время проведения финального грандиозного шоу!"}</p>
				<p>{this.props.lang === "Eng" ? "Each of the finalists will receive the title of \"Miss Queen Europe\" in her country, but only one will win the Main Prize - the Crown of the \"Miss Queen Europe 2019\" and 20 000 €." : " Каждая из финалисток, занявших первое место получит звание «Miss Queen Europe» в своей стране, но только одна получит главный приз - корона и титул «MISS QUEEN EUROPE 2019» и 20 000 Є."}</p>
				<p>{this.props.lang === "Eng" ? "The \"Miss Queen Europe\" European contest is aimed at a charity mission with care for the beauty that surrounds us and inspires us to great things!" : " Европейский конкурс «Miss Queen Europe» направлен на благотворительную миссию с заботой о красоте, которая окружает нас и вдохновляет на великие дела!"}</p>
				<p>{this.props.lang === "Eng" ? "The main mission of the contest is charity and recognition of the beauty of women around the world." : "Основная миссия конкурса - благотворительность и признание красоты женщин всего мира."}</p>
				<p>{this.props.lang === "Eng" ? "Participants of the contest are not just beautiful women, they are personalities and individuals who will make their indisputable contribution to the development of culture, because each of them is a wonderful representative of her country." : " Участниками конкурса являются не просто красивые женщины, это личности и индивидуальности, которые внесут свой неоспоримый вклад в развитие культуры, ведь каждая из них является прекрасным представителем своей страны."}</p>
				<p>{this.props.lang === "Eng" ? "Prizes will be distributed as follows:" : "Призы будут распределены следующим образом:"}</p>
				<ul>
					<li>{this.props.lang === "Eng" ? "1st place - the winner receives a title and a crown, as well as a prize of € 20,000" : "1 место - победитель получает титул и корону, а также приз в размере 20 000 Є"}</li>
					<li>{this.props.lang === "Eng" ? "2nd place - the winner receives a title and a crown, as well as a prize of € 5,000" : "2 место - победитель получает титул и корону, а также приз в 5 000 €"}</li>
					<li>{this.props.lang === "Eng" ? "3rd place - the winner receives a title and a crown, as well as a prize of € 5,000" : "3 место - победитель получает титул и корону, а также приз в размере 5 000 €"}</li>
				</ul>
				<p className="queens-left-align">{this.props.lang === "Eng" ? "Special nomination \"Europe Choice\", which will be held as an online vote during the grand finals on November 2, 2019." : "Специальная номинация «Выбор Европы», которая будет проходить как онлайн голосование во время проведения гранд финала 2 ноября 2019 года."}</p>
				<ul>
					<li>{this.props.lang === "Eng" ? " 4th place - winner in the special nomination \"Europe Choice\" - receives the title and crown, as well as a prize of 5,000 €." : "4 место - победитель в специальной номинации «Выбор Европы» - получает титул и корону, а также приз в размере 5 000 €"}</li>
				</ul>
			</div>
		);
	}
}

export default Header;

