import React, {Suspense} from 'react';
import ReactDOM from 'react-dom';
import Loading from './components/Loading/Loading';
import './index.css';
import * as serviceWorker from './serviceWorker';
const App = React.lazy(() => {
	return new Promise(resolve => {
	  setTimeout(() => resolve(import("./App")), 1000);
	});
  });
ReactDOM.render(<Suspense fallback={<Loading />}><App /></Suspense>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
